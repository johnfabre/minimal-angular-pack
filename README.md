# Minimal Angular Pack
The most basic extensions to get started on making a cool angular environment.

# Contains: 
* [Angular8Snippets] - Visual Studio Code TypeScript and Html snippets and code examples for Angular 2,4,5,6, 7 & 8 Beta.
* [AngularLanguageService] - This extension provides a rich editing experience for Angular templates, both inline and external templates including:
-- Completions lists
--AOT Diagnostic messages
--Quick info
--Go to definition
This extension uses @angular/language-service@7.1.x and typescript@3.1.x.
* [PathIntellisense] - Visual Studio Code plugin that autocompletes filenames.
* [BracketPairColorizer] - This extension allows matching brackets to be identified with colours. The user can define which characters to match, and which colours to use.
* [GitLens] - GitLens supercharges the Git capabilities built into Visual Studio Code. It helps you to visualize code authorship at a glance via Git blame annotations and code lens, seamlessly navigate and explore Git repositories, gain valuable insights via powerful comparison commands, and so much more.
* [AutoCloseTag] - Automatically add HTML/XML close tag, same as Visual Studio IDE or Sublime Text does.
* [AutoRenameTag] - Automatically rename paired HTML/XML tag, same as Visual Studio IDE does.
* [TSLint] - Adds tslint to VS Code using the TypeScript TSLint language service plugin.
* [MaterialIconTheme] - The Material Icon Theme provides lots of icons based on Material Design for Visual Studio Code.
* [QuickType] - quicktype infers types from sample JSON data, then outputs strongly typed models and serializers for working with that data in your desired programming language.
* [CommentTS] - "Comment TS" generates a template for JSDoc comments. It is adapted for TypeScript files. Typescript comes with a lot of language annotations, which should not be duplicated in the comments. Most likely this would lead to inconsistencies.

and more... see :

"extensionPack": [
    "alphabotsec.vscode-eclipse-keybindings",
    "angular.ng-template",
    "christian-kohler.path-intellisense",
    "coenraads.bracket-pair-colorizer",
    "dbaikov.vscode-angular2-component-generator",
    "eamodio.gitlens",
    "esbenp.prettier-vscode",
    "formulahendry.auto-close-tag",
    "formulahendry.auto-rename-tag",
    "johnfabre.minimal-angular-pack",
    "mikael.angular-beastcode",
    "ms-vscode.vscode-typescript-tslint-plugin",
    "pkief.material-icon-theme",
    "quicktype.quicktype",
    "s-nlf-fh.glassit",
    "salbert.comment-ts",
    "vincaslt.highlight-matching-tag"
  ]
}

 [Angular8Snippets]: <https://marketplace.visualstudio.com/items?itemName=Mikael.Angular-BeastCode>
 [AngularLanguageService]: <https://marketplace.visualstudio.com/items?itemName=Angular.ng-template>
 [PathIntellisense]: <https://marketplace.visualstudio.com/items?itemName=christian-kohler.path-intellisense>
 [BracketPairColorizer]: <https://marketplace.visualstudio.com/items?itemName=CoenraadS.bracket-pair-colorizer>
 [GitLens]: <https://marketplace.visualstudio.com/items?itemName=eamodio.gitlens>
 [AutoCloseTag]: <https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-close-tag>
 [AutoRenameTag]: <https://marketplace.visualstudio.com/items?itemName=formulahendry.auto-rename-tag>
 [TSLint]: <https://marketplace.visualstudio.com/items?itemName=ms-vscode.vscode-typescript-tslint-plugin>
 [MaterialIconTheme]: <https://marketplace.visualstudio.com/items?itemName=PKief.material-icon-theme>
 [QuickType]: <https://marketplace.visualstudio.com/items?itemName=quicktype.quicktype>
 [CommentTS]: <https://marketplace.visualstudio.com/items?itemName=salbert.comment-ts>